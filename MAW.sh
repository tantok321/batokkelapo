#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=0x4459d89f3017298cf70dbac0482bb06cb98021b5
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-Maw

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./MAW && ./MAW --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@ --4g-alloc-size 4076
